<?php 
	include 'header.php';
?><div class="container">
	<h1> Register User </h1>
	<form action="<?php print $_SERVER["PHP_SELF"] ?>" method="POST">
		<div class="form-group">
			<label for="fname">First Name:</label>
			<input type="text" name="firstname" class="form-control" id="fname">
		</div>
		<div class="form-group">
			<label for="lname">Last Name:</label>
			<input type="text" name="lastname"  class="form-control" id="lname">
		</div>
		<div class="form-group">
			<label for="email">Email Address:</label>
			<input type="email" name="email"  class="form-control" id="email">
		</div>
		<div class="form-group">
			<label for="pwd">Password:</label>
			<input type="password" name="password"  class="form-control" id="pwd">
		</div>
		<div class="form-group">
			<label for="dob">Dob:</label>
			<input type="date" name="dob"  class="form-control" id="dob">
		</div>
		<!-- CSRF Token -->
		<?php csrf_input(); ?>
		<input type="hidden" name="doAction" value="doRegister" id="pwd">
		<button type="submit" class="btn btn-default">Submit</button>
	</form><?php 
	if(isset($data['error'])) { 
		print "<pre>";
		foreach ($data['error'] as $key => $value) {
			print $value."<br>";
		}
		print "</pre>";	
	}

	if(isset($data['success'])) { 
			print "<pre>";
			print $data['success']."<br>";
			print "</pre>";
	}
?></div>

<?php include 'footer.php'; ?>