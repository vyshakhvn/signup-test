<?php
namespace Models;

class DBResult {

    static private $link = null;
    static public $connection_info = array();
    static private $where;
    static private $limit;
    static private $order;  


    function __construct(){

        self::$connection_info = array('host' => HOST, 'user' => DB_USERNAME, 'pass' => DB_PASSWORD, 'db' => DATABASE);

    }

    function __destruct(){
        if(is_resource(self::$link)) mysqli_close(self::$link);
    }


    /**
     * Create or return a connection to the MySQL server.
     */

    private static function connection(){
        if(!is_resource(self::$link) || empty(self::$link)){
            if(($link = mysqli_connect(self::$connection_info['host'], self::$connection_info['user'], self::$connection_info['pass'], self::$connection_info['db']))){
                self::$link = $link;
                mysqli_set_charset(self::$link, 'utf8');
            }else{
                throw new Exception('Could not connect to MySQL database.');
            }
        }
        return self::$link;
    }



    /**
     * MySQL Where methods
     */

    private static function __where($info, $type = 'AND'){
        $link = self::connection();
        $where = self::$where;
        foreach($info as $row => $value){
            if(empty($where)){
                $where = sprintf("WHERE `%s`='%s'", $row, mysqli_real_escape_string($link ,$value));
            }else{
                $where .= sprintf(" %s `%s`='%s'", $type, $row, mysqli_real_escape_string($link , $value));
            }
        }
        self::$where = $where;
    }

    /**
     * Function  to attach sql WHERE fields
     */

    public function where($field, $equal = null){

        if(is_array($field)){
            self::__where($field);
        }else{
           self::__where(array($field => $equal));
        }
        return $this;
    }


    /**
     * Function to fetch data from table
     */

    public function get($table, $select = '*'){
        $link = self::connection();
        if(is_array($select)){
            $cols = '';
            foreach($select as $col){
                $cols .= "`{$col}`,";
            }
            $select = substr($cols, 0, -1);
        }
        $sql = sprintf("SELECT %s FROM %s%s", $select, $table, self::extra());
        
        $result = mysqli_query($link, $sql);
    
        if(!($result = mysqli_query($link, $sql))){
            throw new Exception('Error executing MySQL query: '.$sql.'. MySQL error '.mysqli_errno().': '.mysqli_error());
            $data = false;
        }elseif(is_object($result)){
            $num_rows = mysqli_num_rows($result);
            if($num_rows === 0){
                $data = false;
            }elseif(preg_match('/LIMIT 1/', $sql)){
                $data = mysqli_fetch_assoc($result);
            }else{
                $data = array();

                while($row = mysqli_fetch_assoc($result)){
                    $data[] = $row;
                }
            }
        }else{
            $data = false;
        }
        mysqli_free_result($result);
        return $data;
    }

    /**
     * MySQL query helper
     */

    static private function extra(){
        $extra = '';
        if(!empty(self::$where)) $extra .= ' '.self::$where;
        if(!empty(self::$order)) $extra .= ' '.self::$order;
        if(!empty(self::$limit)) $extra .= ' '.self::$limit;
        // cleanup
        self::$where = null;
        self::$order = null;
        self::$limit = null;
        return $extra;
    }

    /**
     * Function to insert query 
     */

    public function insert($table, $data){

        $link = self::connection();
        $fields = '';
        $values = '';

        foreach($data as $col => $value){
            $fields .= sprintf("`%s`,", $col);
            $values .= sprintf("'%s',", mysqli_real_escape_string($link, $value));
        }

        $fields = substr($fields, 0, -1);
        $values = substr($values, 0, -1);
        $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $table, $fields, $values);
        
        if(mysqli_query($link, $sql)){
            return true;
        }else{
            return false;
        }
    }

}
?>