# SignUp Test

## Installation

- Configure DB git_test.sql
- Install Packages
```sh
composer install
```
- Run server in CLI
```sh
php -S localhost:8000 router.php
```
- In browser load http://localhost:8000/register , http://localhost:8000/login
- To run unit test files
```sh
php test.php
```

