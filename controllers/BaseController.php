<?php

namespace Controllers; 
use Models\DBResult;
use Exception;


class BaseController 
{	
	protected static $arrRegisteredDoActions;
	protected $obj;
	protected static $DB;

	/*
     * Initializes variables.
     * @return void
     */
    function __construct()
    {	
    	self::$DB = new DBResult();
    	self::$arrRegisteredDoActions = getRegisteredDoActions();
    	self::processRequest();

    }

    /*
     * Process the incoming request.
     * @return void
     */

    static function processRequest(){

		$uri = urldecode(
			    	parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
				);
		$uri = ltrim($uri, '/');

    	if(self::checkLoginStatus()){
    		if(!empty($_REQUEST['doAction'])){
				$uri = $_REQUEST['doAction'];
			}
			else{
				$uri = 'home';
			}
		}
		else{
			$arrNoAuthRoutes = [ 'doRegister', 'register','login', 'logMeIn' ];
 			if(isset($_REQUEST['doAction']) && in_array($_REQUEST['doAction'], $arrNoAuthRoutes)){
 				$uri = $_REQUEST['doAction'];	
 			}
 			elseif(empty($_REQUEST['doAction']) && in_array($uri, $arrNoAuthRoutes)) {
 				$uri = $uri;
 			}
 			else {
 				$uri = 'login';
 			}
		}	
		
    	self::resolveUri($uri);
	    self::callRoute($uri);
    	/*|| isset($_POST['doAction']) && 
    		$_POST['doAction'] != 'logMeIn'*/
    }


    /*
     * Calls the route registered in doActions.php. Will initialze the controller & calls 	  the function.
     * @return void
     */

    static function callRoute($uri){

		$obj = new self::$arrRegisteredDoActions[$uri][0]();
		call_user_func([
			$obj,
			self::$arrRegisteredDoActions[$uri][1]
		]);
    }

    /*
     * Validates the incoming request URI
     * @return Exception $e || void
     */

    static function resolveUri($uri){

		if(!isset(self::$arrRegisteredDoActions[$uri])){
			throw new Exception("URI not registered!.");
		}

		if(!class_exists(self::$arrRegisteredDoActions[$uri][0])){
			throw new Exception("Method " . self::$arrRegisteredDoActions[$uri][1] ." not found! in " . self::$arrRegisteredDoActions[$uri][0]);
		}

		if(!method_exists(self::$arrRegisteredDoActions[$uri][0], self::$arrRegisteredDoActions[$uri][1])){
			throw new Exception("Method " . self::$arrRegisteredDoActions[$uri][1] ." not found! in " . self::$arrRegisteredDoActions[$uri][0]);
		}
		if ($_SERVER['REQUEST_METHOD'] !== self::$arrRegisteredDoActions[$uri][2]) {
     		throw new Exception("Method " . self::$arrRegisteredDoActions[$uri][1] ." not found! in " . self::$arrRegisteredDoActions[$uri][0]);
		}
	}

	/*
     * Checks user is logged in or not
     * @return Exception $e || void
     */

    static function checkLoginStatus(){
    	if(!empty($_SESSION['USER_ID']))
    		return true;
    	else
    		return false;
    }

}	