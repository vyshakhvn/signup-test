<?php

namespace Controllers;
use Models\DBResult;

class HomeController 
{
	public $DB;

	/*
     * Constructor for initialzing variables
     */
	function __construct(){	
		$this->DB = new DBResult();
    }

    /*
     * Home view
     */
    function home(){
    	return showHTML('home');
    }

    /*
     * login view
     */
    function login(){

    	return showHTML('login');
    }	

    /*
     * Function to check login user.
     */
    function logInUser(){

    	validateCsrfToken();
    	$data = [];
    	$validateInputs = [ 'email', 'password'];

    	foreach ($validateInputs as $input) {
    		if(empty($_REQUEST[$input])){
    			$data['error'][$input] = ucfirst($input) . " field required!";
    		}
    		else{
    			// Email Validation
    			if($input === 'email'){
	    			if (!filter_var($_REQUEST[$input], FILTER_VALIDATE_EMAIL)) {
	  					$data['error'][$input.'_invalid'] = ucfirst($input) . " field is invalid!"; 
					}
				}

				if($input === 'password'){
					$minLength = 4;
	    			if (strlen($_REQUEST[$input]) < $minLength ) {
	  					$data['error'][$input.'_invalid'] = ucfirst($input) . " field must have a minimum length of " . $minLength . "!"; 
					}
				}

    		}
    	}

    	if(isset($data['error']))
    		return showHTML('login', $data);
    	else{

    		$data = [
    			'email' => $_REQUEST['email'],
    			'password' => sha1($_REQUEST['password']),
    		];
    		$user = $this->DB->where($data)->get('users')[0];
    		if(isset($user['id'])){
    			$_SESSION['USER_ID'] = $user['id'];
    			$_SESSION['NAME'] = $user['fname'].' '.$user['lname'];
    			header('Location: home');
    		}
   			else {
   				$data['success'] = "User not found!"; 
   				return showHTML('login', $data);
   			}
    	}
    }

    /*
     * register view
     */

    function register(){
    	return showHTML('register');
    }

    /*
     * Function to register user.
     */
    function registerUser(){

    	validateCsrfToken();
    	$data = [];
    	$validateInputs = [  'firstname', 'lastname', 'email', 'password', 'dob' ];

    	foreach ($validateInputs as $input) {
    		if(empty($_REQUEST[$input])){
    			$data['error'][$input] = ucfirst($input) . " field required!";
    		}
    		else{
    			// Email Validation
    			if($input === 'email'){
	    			if (!filter_var($_REQUEST[$input], FILTER_VALIDATE_EMAIL)) {
	  					$data['error'][$input.'_invalid'] = ucfirst($input) . " field is invalid!"; 
					}
				}

				if($input === 'password'){
					$minLength = 4;
	    			if (strlen($_REQUEST[$input]) < $minLength ) {
	  					$data['error'][$input.'_invalid'] = ucfirst($input) . " field must have a minimum length of " . $minLength . "!"; 
					}
				}

    		}
    	}

    	if(isset($data['error']))
    		return showHTML('register', $data);
    	else{

    		$data = [
    			'fname' => $_REQUEST['firstname'],
    			'lname' => $_REQUEST['lastname'],
    			'email' => $_REQUEST['email'],
    			'dob' => $_REQUEST['dob'],
    			'password' => sha1($_REQUEST['password']),
    		];
   			if($this->DB->insert('users', $data)){
   				$data['success'] = "User registered successfully!"; 
   				return showHTML('register', $data);
   			}
   			else {
   				return showHTML('register', $data);
   			}
    	}
    }

    /*
     * Function to check logout user.
     */

    function logout(){
    	session_unset('USER_ID');
    	session_unset('csrfToken');
    	session_destroy();
    	header('Location: login');
    }

}	


?>