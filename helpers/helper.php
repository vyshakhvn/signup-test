<?php

/*
 * Function to generate CSRF token
 */

if(!function_exists("generateCsrfToken")) {
    
    function generateCsrfToken()
    {	
    	if(empty(APP_KEY)){
    		throw new Exception('App Key is not defined');
    	}
        if(empty($_SESSION['csrfToken'])) {
        	$rand = openssl_random_pseudo_bytes(32); 
		    $_SESSION['csrfToken'] = substr(base64_encode(hash_hmac('sha256', $rand, APP_KEY)), 0, 45);
		}  

		return $_SESSION['csrfToken'];
    }
}

/*
 * Function to validate CSRF token
 */

if(!function_exists("validateCsrfToken")) {
    
    function validateCsrfToken()
    {	
    	if(!isset($_REQUEST['csrf_token']) || $_REQUEST['csrf_token'] != generateCsrfToken()){
	    	throw new Exception("CSRF Token Mismatch");
	    }
    }
}

/**
 * Generates a CSRF token form field for view files.
 */

if(! function_exists('csrf_input')) {
    
    function csrf_input()
    {
        print html_entity_decode('&lt;input type=&quot;hidden&quot; name=&quot;csrf_token&quot; value=&quot;'.generateCsrfToken().'&quot;&gt;');
    }
}


/*
 * Realpath to config folder
 */

if(!function_exists("getConfigPath")) {
    
    function getConfigPath()
    {
        return CONFIGPATH;
    }
}

/*
 * Will returns the Registered routes
 */

if(!function_exists("getRegisteredDoActions")) {
    
    function getRegisteredDoActions()
    {
        return include CONFIGPATH.DIRECTORY_SEPARATOR.'DoActions.php';
    }
}


/*
 * Will include  the view file
 */

if(!function_exists("showHTML")) {
    
    function showHTML($fileName, $data = array())
    {
        return include VIEWPATH.DIRECTORY_SEPARATOR. $fileName . '.php';
    }
}
