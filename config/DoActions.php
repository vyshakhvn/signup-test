<?php

/*
| Defines the general routing here & doAction parameter.
| Syntax 'doAction' => [ 'Controllers\YourController', 'your_function', 'Method' ]
*/
return [

	'login' => [ 'Controllers\HomeController', 'login', 'GET' ],
	'logout' => [ 'Controllers\HomeController', 'logout', 'GET' ],
	'logMeIn' => [ 'Controllers\HomeController', 'logInUser', 'POST' ],
	
	'register' => [ 'Controllers\HomeController', 'register', 'GET' ],
	'doRegister' => [ 'Controllers\HomeController', 'registerUser', 'POST' ],

	'home' => [ 'Controllers\HomeController', 'home', 'GET' ],
];

?>