<?php
use PHPUnit\Framework\TestCase;
require_once 'includeTestFiles.php';


use Controllers\HomeController;

class LoginTest extends TestCase
{   
    /**
     * @runInSeparateProcess
    **/
    public function testlogInUser()
    {   
        session_start();
        $obj =  new HomeController();
        $_REQUEST['csrf_token'] = generateCsrfToken();
        $_REQUEST['email'] = 'test@gmail.com';
        $_REQUEST['password'] = 'test@gmail.com';
        $obj->logInUser();
        $this->assertNotEmpty($_SESSION['USER_ID']);
        $obj->logout();
    }

}