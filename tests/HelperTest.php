<?php
use PHPUnit\Framework\TestCase;
require_once 'includeTestFiles.php';

class CSRFTokenTest extends TestCase
{
    public function testGenerateCsrfToken()
    {   
        $token = generateCsrfToken();
        $this->assertIsString(generateCsrfToken());
        $this->assertEquals(45, strlen(generateCsrfToken()));
        return $token;
    }

    /**
     * @depends testGenerateCsrfToken
     */
    public function testValidateCsrfToken(string $token)
    {   
        $this->assertEquals($token, generateCsrfToken());
    }
}