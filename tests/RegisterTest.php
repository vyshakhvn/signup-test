<?php
use PHPUnit\Framework\TestCase;
require_once 'includeTestFiles.php';
use Controllers\HomeController;

class RegisterTest extends TestCase
{   
    /**
     * @runInSeparateProcess
    **/
    public function testRegisterUser()
    {   
        session_start();
        $obj =  new HomeController();
        $_REQUEST['csrf_token'] = generateCsrfToken();
        $_REQUEST['firstname'] = 'test';
        $_REQUEST['lastname'] = 'vn';
        $_REQUEST['email'] = 'test@gmail.com';
        $_REQUEST['password'] = 'test@gmail.com';
        $_REQUEST['dob'] = '2019-01-20';
        $obj->registerUser();
        $data = [
            'email' => $_REQUEST['email'],
            'password' => sha1($_REQUEST['password']),
        ];
        $user = $obj->DB->where($data)->get('users')[0];
        $this->assertEquals($_REQUEST['email'],$user['email']);
        session_destroy();
    }

}