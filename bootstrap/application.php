<?php

/*
| Application File will initialize a call to BaseController. 
| Also it will Generate CSRF Token
*/

use Controllers\BaseController;

session_start();
ini_set('session.gc_maxlifetime', 3600);
generateCsrfToken();

$req = new BaseController();