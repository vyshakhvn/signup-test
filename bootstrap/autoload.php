<?php

/*
| Will Autoload Classes. Load helper Files.
*/

function my_autoloader($class) {

	$url = explode('\\', $class);
	$namespace = str_replace(end($url), "", $class);
	$file = strtolower($namespace) . end($url) . '.php';
	if(file_exists($file))
    	include $file;
    else
    	die("$class File not found! (404)");	
}

spl_autoload_register('my_autoloader');

require_once __DIR__.'/../helpers/helper.php';
require_once __DIR__.'/../config/AppConfig.php';
?>